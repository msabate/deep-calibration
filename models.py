import torch
import torch.nn as nn
import numpy
import math




class Net_timestep(nn.Module):
    
    def __init__(self, dim, nOut, n_layers, vNetWidth, activation = "relu"):
        super(Net_timestep, self).__init__()
        self.dim = dim
        self.nOut = nOut
        
        if activation!="relu" and activation!="tanh":
            raise ValueError("unknown activation function {}".format(activation))
        if activation == "relu":
            self.activation = nn.ReLU()
        else:
            self.activation = nn.Tanh()
        
        self.i_h = self.hiddenLayerT1(dim, vNetWidth)
        self.h_h = nn.ModuleList([self.hiddenLayerT1(vNetWidth, vNetWidth) for l in range(n_layers-1)])
        self.h_o = self.outputLayer(vNetWidth, nOut)
        
    def hiddenLayerT0(self,  nIn, nOut):
        layer = nn.Sequential(nn.BatchNorm1d(nIn, momentum=0.1), nn.Linear(nIn,nOut,bias=True),
                              nn.BatchNorm1d(nOut, momentum=0.1),   
                              self.activation)   
        return layer
    
    def hiddenLayerT1(self, nIn, nOut):
        layer = nn.Sequential(nn.Linear(nIn,nOut,bias=True),
                              #nn.BatchNorm1d(nOut, momentum=0.1),  
                              self.activation)   
        return layer
    
    
    def outputLayer(self, nIn, nOut):
        layer = nn.Sequential(nn.Linear(nIn, nOut,bias=True))
        return layer
    
    def forward(self, S):
        h = self.i_h(S)
        for l in range(len(self.h_h)):
            h = self.h_h[l](h)
        output = self.h_o(h)
        return output



class Net_SDE(nn.Module):
    
    def __init__(self, dim, timegrid, n_layers, vNetWidth, device):
        
        super(Net_SDE, self).__init__()
        self.dim = dim
        self.timegrid = torch.tensor(timegrid).to(device)
        self.device = device
        
        # input to the networks will be S_t, \sigma, \mu, \rho)
        self.drift = nn.ModuleLis([Net_timestep(dim=dim+3, nOut=1, n_layers=n_layers, vNetWidth=vNetWidth) for t in timegrid[:-1]])
        self.volatility = nn.ModuleList([Net_timestep(dim=dim+3, nOut=1, n_layers=n_layers, vNetWidth=vNetWidth) for t in timegrid[:-1]])
        
    def forward(self, S0, sigma, mu, rho, K, MC_samples):
        batch_size = S0.shape[0]
        S_old = torch.repeat_interleave(S0, MC_samples, dim=0)

        sigma_extended = torch.repeat_interleave(sigma, MC_samples, dim=0)
        mu_extended = torch.repeat_interleave(mu, MC_samples, dim=0)
        rho_extended = torch.repeat_interleave(rho, MC_samples, dim=0)
        
        K_extended = torch.repeat_interleave(K, MC_samples, dim=0)


        for i in range(1, len(self.timegrid)):
            h = self.timegrid[i]-self.timegrid[i-1]
            dW = torch.sqrt(h) * torch.randn(MC_samples, 1, device=self.device)
            dW = dW.repeat(batch_size, 1)
            
            input_network = torch.cat([S_old, sigma_extended, mu_extended, rho_extended],1)
            S_new = S_old + self.drift[i-1](input_network)*h + self.volatility[i-1](input_network)*dW
            
            S_old = S_new
        
        zeros = torch.zeros_like(S_old)
        price = torch.cat([S_old - K_extended, zeros],1)
        price = torch.max(price, 1, keepdim=True)
        price_split = torch.split(price, MC_samples)
        avg_price = torch.cat([p.mean().view(1,1) for p in price_split], 0)
        
        return avg_price

        
    




